package net.nilosplace.gff;

import java.io.File;
import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import net.nilosplace.gff.entity.GeneFeature;
import net.nilosplace.gff.entity.IntervalTree;
import net.nilosplace.neo4j.ConfigHelper;

public class Main {

	private static Logger log = LogManager.getLogger(Main.class);

	public static void main(String[] args) throws Exception {
		ConfigHelper.init();

		ArrayList<String> fileList = new ArrayList<String>() {{
			add("gff/processed/RGD_1.0.1_9606.gff3"); // 3674931
			add("gff/processed/dmel-all-no-analysis-r6.13.gff"); // 2292229
			add("gff/processed/RGD_1.0.1_10116.gff3"); // 1538771
			add("gff/processed/zfin_genes.gff3"); // 1132235
			add("gff/processed/c_elegans.PRJNA13758.WS258.genes_only.gff3"); // 834780
			add("gff/processed/MGI_GenomeFeature_for_JBrowse.gff3"); // 341862
			add("gff/processed/saccharomyces_cerevisiae.gff"); // 23058
		}};

		ArrayList<GeneFeature> features = new ArrayList<GeneFeature>();

		for(String file: fileList) {
			GFF gff = new GFF(new File(file));
			gff.load();
			features.addAll(gff.features);
			log.info("Finished Adding: " + gff.features.size());
			gff.features.clear();
		}

		IntervalTree<GeneFeature> tree = new IntervalTree<GeneFeature>();
		log.info("Building Tree:");
		tree.buildTree(features);
		log.info("Building Tree Finished:");

		log.info(tree);

		//Thread.sleep(10000000);

		//System.out.println(it);
		//Date start = new Date();
		//log.info(tree.query(new Interval(29000, 30000)).size());
		//Date end = new Date();
		//log.info("Diff in ms: " + (end.getTime() - start.getTime()));
		//log.info("Total Size: " + features.size());

	}
}