package net.nilosplace.gff.entity;

import java.util.HashMap;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString(callSuper=true)
public class GeneFeature extends Interval {

	public GeneFeature() { super(0, 0); }

	public GeneFeature(long start, long end) {
		super(start, end);
	}

	private static HashMap<String, String> cache = new HashMap<String, String>();
	private String seqname;
	private String source;
	private String feature;

	private String score;
	private String strand;
	private String frame;

	private final HashMap<String, String> attributes = new HashMap<String, String>();

	public void setSeqname(String seqname) {
		this.seqname = getKey(seqname);
	}
	public void setSource(String source) {
		this.source = getKey(source);
	}
	public void setFeature(String feature) {
		this.feature = getKey(feature);
	}
	public void setScore(String score) {
		this.score = getKey(score);
	}
	public void setFrame(String frame) {
		this.frame = getKey(frame);
	}
	public void setStrand(String strand) {
		this.strand = getKey(strand);
	}

	public String getAttribute() {
		StringBuffer buffer = new StringBuffer();
		String delim = "";
		for(String key: attributes.keySet()) {
			buffer.append(delim + key + "=" + attributes.get(key));
			delim = ";";
		}
		return buffer.toString();
	}
	public void setAttribute(String attribute) {
		String[] attribs = attribute.split(";");
		for(String attrib: attribs) {
			String[] keyValue = attrib.split("=");
			if(keyValue.length > 1) {
				String key = getKey(keyValue[0]);
				String value = getKey(keyValue[1]);

				attributes.put(key, value);
			}
		}
	}

	public String getKey(String key) {
		if(!cache.containsKey(key)) {
			cache.put(key, key);
		}
		return cache.get(key);
	}

}
