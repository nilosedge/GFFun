package net.nilosplace.gff.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @ToString
public abstract class Interval implements Comparable<Interval> {

	private long start;
	private long end;

	public Interval(long start, long end) {
		this.start = start;
		this.end = end;
	}

	@Override
	public int compareTo(Interval other) {
		if (start < other.getStart())
			return -1;
		else if (start > other.getStart())
			return 1;
		else if (end < other.getEnd())
			return -1;
		else if (end > other.getEnd())
			return 1;
		else
			return 0;
	}
}
