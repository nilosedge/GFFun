package net.nilosplace.gff.entity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedSet;
import java.util.TreeSet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.neo4j.driver.v1.AuthTokens;
import org.neo4j.driver.v1.Driver;
import org.neo4j.driver.v1.GraphDatabase;
import org.neo4j.driver.v1.Session;
import org.neo4j.driver.v1.Statement;
import org.neo4j.driver.v1.Values;

import net.nilosplace.neo4j.ConfigHelper;

public class IntervalTree<T extends Interval> {

	private IntervalNode<T> treeRoot;
	protected Driver driver;
	protected Session session;
	private static Logger log = LogManager.getLogger(IntervalTree.class);
	private final Statement createSearchNodeStatement = new Statement("CREATE (n:SearchNode {primaryId: $primaryId})");
	private final Statement createIntervalNodeStatement = new Statement(
			"CREATE (n:IntervalNode {seqname: $seqname, source: $source, feature: $feature, score: $score, strand: $strand, frame: $frame, start: $start, end: $end}) WITH n"
			+ " MATCH (m:SearchNode) WHERE m.primaryId = $primaryId"
			+ " CREATE (m)<-[:INTERVAL]-(n)");

	private final Statement createLeftStatement = new Statement(" MATCH (p:SearchNode), (c:SearchNode) WHERE p.primaryId = $parent AND c.primaryId = $child CREATE (p)-[:LEFT]->(c)");
	private final Statement createRightStatement = new Statement(" MATCH (p:SearchNode), (c:SearchNode) WHERE p.primaryId = $parent AND c.primaryId = $child CREATE (p)-[:RIGHT]->(c)");

	public IntervalTree() {
		if(session == null) {

			driver = GraphDatabase.driver("bolt://" + ConfigHelper.getNeo4jHost() + ":" + ConfigHelper.getNeo4jPort(), AuthTokens.basic("neo4j", "getmeon"));

			session = driver.session();
		}
	}

	public void buildTree(List<T> intervalList) {
		treeRoot = new IntervalNode<>();
		buildTree(treeRoot, intervalList);
	}

	private void buildTree(IntervalNode<T> root, List<T> intervalList) {

		SortedSet<Long> endpoints = new TreeSet<Long>();

		for (Interval interval : intervalList) {
			endpoints.add(interval.getStart());
			endpoints.add(interval.getEnd());
		}

		long median = getMedian(endpoints);
		root.setCenter(median);
		//center = median;

		List<T> left = new ArrayList<T>();
		List<T> right = new ArrayList<T>();

		for (T interval: intervalList) {
			if (interval.getEnd() < median) {
				left.add(interval);
			} else if (interval.getStart() > median) {
				right.add(interval);
			} else {
				List<T> posting = root.getIntervals().get(interval);
				if (posting == null) {
					posting = new ArrayList<T>();
					root.getIntervals().put(interval, posting);
				}
				posting.add(interval);
			}
		}
		intervalList.clear();

		if (left.size() > 0) {
			root.setLeftNode(new IntervalNode<T>());
			buildTree(root.getLeftNode(), left);
		}
		if (right.size() > 0) {
			root.setRightNode(new IntervalNode<T>());
			buildTree(root.getRightNode(), right);
		}
	}

	public List<T> query(Interval target) {
		return query(treeRoot, target);
	}

	public List<T> query(IntervalNode<T> root, Interval target) {
		List<T> result = new ArrayList<T>();

		for (Entry<T, List<T>> entry : root.getIntervals().entrySet()) {
			if (intersects(entry.getKey(), target)) {
				for (T interval : entry.getValue()) {
					result.add(interval);
				}
			} else if (entry.getKey().getStart() > target.getEnd()) {
				break;
			}
		}

		if (target.getStart() < root.getCenter() && root.getLeftNode() != null) {
			result.addAll(query(root.getLeftNode(), target));
		}
		if (target.getEnd() > root.getCenter() && root.getRightNode() != null) {
			result.addAll(query(root.getRightNode(), target));
		}
		return result;
	}

	public boolean intersects(Interval source, Interval target) {
		return source.getEnd() > target.getStart() && source.getStart() < target.getEnd();
	}

	private Long getMedian(SortedSet<Long> set) {
		int i = 0;
		int middle = set.size() / 2;
		for (Long point : set) {
			if (i == middle)
				return point;
			i++;
		}
		return null;
	}

	@Override
	public String toString() {
		log.info("Deleting all nodes");
		session.run("MATCH (n) DETACH DELETE n").list();
		printNode(treeRoot);
		session.close();
		driver.close();
		return "";
	}

	private Long printNode(IntervalNode<T> root) {

		session.run(createSearchNodeStatement.withParameters(Values.parameters( "primaryId", root.getCenter())));

		for (Entry<T, List<T>> entry : root.getIntervals().entrySet()) {
			for (Interval interval : entry.getValue()) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("primaryId", root.getCenter());
				GeneFeature f = (GeneFeature)interval;
				map.put("seqname", f.getSeqname());
				map.put("source", f.getSource());
				map.put("feature", f.getFeature());
				map.put("score", f.getScore());
				map.put("strand", f.getStrand());
				map.put("frame", f.getFrame());
				map.put("start", f.getStart());
				map.put("end", f.getEnd());
				session.run(createIntervalNodeStatement.withParameters(map));
				map.clear();
			}
			entry.getValue().clear();
		}

		if(root.getLeftNode() != null) {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("parent", root.getCenter());
			map.put("child", printNode(root.getLeftNode()));
			session.run(createLeftStatement.withParameters(map));
			map.clear();
		}
		if(root.getRightNode() != null) {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("parent", root.getCenter());
			map.put("child", printNode(root.getRightNode()));
			session.run(createRightStatement.withParameters(map));
			map.clear();
		}

		return root.getCenter();
	}

}
