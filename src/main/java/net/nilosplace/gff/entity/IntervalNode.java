package net.nilosplace.gff.entity;

import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class IntervalNode<T extends Interval> {

	private SortedMap<T, List<T>> intervals;
	private long center = 0;
	private IntervalNode<T> leftNode;
	private IntervalNode<T> rightNode;

	public IntervalNode() {
		intervals = new TreeMap<T, List<T>>();
		leftNode = null;
		rightNode = null;
	}
}
