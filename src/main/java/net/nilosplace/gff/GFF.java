package net.nilosplace.gff;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.fasterxml.jackson.dataformat.csv.CsvParser.Feature;

import net.nilosplace.gff.entity.GeneFeature;

public class GFF {

	private static Logger log = LogManager.getLogger(GFF.class);

	private final File file;
	public List<GeneFeature> features = new ArrayList<GeneFeature>();

	public GFF(File file) {
		this.file = file;
	}

	public void load() {
		log.info("Loading file: " + file.getAbsolutePath());
		String line = null;
		int c = 0;
		GeneFeature f = null;
		try {
			BufferedReader reader = new BufferedReader(new FileReader(file));
			CsvMapper mapper = new CsvMapper();

			mapper.configure(Feature.IGNORE_TRAILING_UNMAPPABLE, true);

			CsvSchema schema = CsvSchema.builder()
					.setColumnSeparator('\t')
					//.setAllowComments(true)
					//.setNullValue("null")
					.addColumn("seqname")
					.addColumn("source")
					.addColumn("feature")
					.addColumn("start", CsvSchema.ColumnType.NUMBER)
					.addColumn("end", CsvSchema.ColumnType.NUMBER)
					.addColumn("score")
					.addColumn("strand")
					.addColumn("frame")
					.addColumn("attribute")
					.build();

			while((line = reader.readLine()) != null) {
				if(line.startsWith("##FASTA")) {
					c++;
					break;
				}
				if(line.startsWith("#")) {
					c++;
					continue;
				}

				//System.out.println(line);
				if(c % 1000000 == 0) {
					log.info("Amount: " + c);
				}

				f = mapper.readerFor(GeneFeature.class).with(schema).readValue(line);
				if(f.getStart() > f.getEnd()) {
					//long temp = f.getEnd();
					//f.setEnd(f.getStart());
					//f.setStart(temp);
					log.info(file.getAbsolutePath() + " " + f);
				} else {
					features.add(f);
				}
				c++;
			}
			log.info("Loading Finished: " + features.size());
		} catch (Exception e) {
			log.info(f);
			log.info(c + " --> " + line);
			e.printStackTrace();
		}

	}

}
